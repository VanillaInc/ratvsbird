﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolunteerManager : MonoBehaviour {

    private CannonShooter cs;

    public GameObject[] _volunteers;
    public VolunteerVar[] volunteers;
    private Color birdColor;

    // Use this for initialization
    void Start () {
        cs = FindObjectOfType<CannonShooter>();

        for(int i = 0; i < volunteers.Length; i++)
        {
            volunteers[i].index = i + 1;
            volunteers[i].bird = _volunteers[i];
        }

        birdColor = _volunteers[0].GetComponent<SpriteRenderer>().color;
    }
	
	// Update is called once per frame
	void Update () {
        foreach (VolunteerVar vv in volunteers)
        {
            VolunteerController vc = vv.bird.GetComponent<VolunteerController>();

            if (vv.index > cs.currentAmmo)
            {
                if (!vv.egg)
                {
                    vc.Egg();
                    vv.egg = true;
                }
            }

            if(vv.index <= cs.currentAmmo)
            {
                if(vv.egg)
                {
                    vc.Idle();
                    vv.egg = false;
                }
            }

            vv.bird.SetActive(vv.index <= cs.maxAmmo);
        }

        if (cs.timeBetweenReloads > 0)
        {
            foreach (VolunteerVar v in volunteers)
            {
                if (v.egg)
                {
                    Color currentcolor = v.bird.GetComponent<SpriteRenderer>().color;
                    v.bird.GetComponent<SpriteRenderer>().color = new Color(1, currentcolor.g, currentcolor.b);
                }
            }
        }
        else
        {
            foreach (VolunteerVar v in volunteers)
            {
                v.bird.GetComponent<SpriteRenderer>().color = birdColor;
            }
        }
	}

    public void Refresh()
    {
        foreach (VolunteerVar vv in volunteers)
        {
            vv.bird.GetComponent<VolunteerController>().Egg();
            vv.egg = true;
        }
    }
}

[System.Serializable]
public class VolunteerVar
{
    public GameObject bird;
    public int index;
    public bool egg;
}