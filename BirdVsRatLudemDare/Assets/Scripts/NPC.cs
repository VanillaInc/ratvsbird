﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPC : MonoBehaviour {

    public Dialogue[] dialogue;
    bool transition;
    //public Image img;

    private void Start()
    {
        transition = GetComponent<DialogueToWave>() != null;
    }

    public void TriggerDialogue()
    {
        FindObjectOfType<DialogueManager>().StartNewDialogueBranch(dialogue, transition, gameObject);
    }

}