﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour {

    public GameObject gameOver;

    public TMP_Text score;
    public Slider health;
    public TMP_Text TTNR;
    public TMP_Text perfect;

    public Transform healthSpawn;

    public int healthToAdd;

    public ParticleSystem heal;

    public GameObject upgradeMenu;

    private CannonController cc;

    private void Start()
    {
        cc = FindObjectOfType<CannonController>();
    }

    private void Update()
    {
        score.text = GetComponent<ScoreManager>().score.ToString();
        health.value = GetComponent<HealthManager>().value;
        health.maxValue = GetComponent<HealthManager>().startValue;
    }

    public void Heal()
    {
        Instantiate(heal, healthSpawn.position, Quaternion.identity);
        GetComponent<HealthManager>().AddHealth(healthToAdd);
    }

    public void GameOver()
    {
        gameOver.SetActive(true);
    }

    public void OpenUpgradeMenu()
    {
        upgradeMenu.SetActive(true);
    }
}
