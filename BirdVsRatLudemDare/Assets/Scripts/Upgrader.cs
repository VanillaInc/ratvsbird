﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrader : MonoBehaviour {

    public ParticleSystem collect;
    public AudioSource spawn;
    public AudioSource collectSound;

    private void Start()
    {
        collectSound = transform.GetChild(0).GetComponent<AudioSource>();
    }

    void Update () {
        transform.parent.transform.Translate(Vector3.down * 2 * Time.deltaTime);
	}
	
	void OnMouseDown () {
        Collect();
    }

    void Collect()
    {
        collectSound.Play();
        UpgradeManager.upgrading = true;
        Instantiate(collect, transform.position, Quaternion.identity);
        FindObjectOfType<CannonController>().Upgrade();
        Destroy(gameObject);
    }
}
