﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UpgradeManager : MonoBehaviour {

    public static bool firstUpgradeDone;
    public Animator firstUp;

    public float _timeToUpgrade;
    private float timeToUpgrade;
    public TMP_Text timer;

    public int healthUpgrades;
    public int maxAmmoUpgrades;

    public int[] scoreToUpgrades;
    public int STUindex;

    public GameObject[] upgradeButtons;

    public static bool upgrading;

    private CannonController cc;

    private void Start()
    {
        cc = FindObjectOfType<CannonController>();
    }

    private void Update()
    {
        timer.text = timeToUpgrade.ToString("F3");

        if(!firstUpgradeDone && upgrading)
        {
            firstUp.Play("peek");
            firstUpgradeDone = true;
        }

        if (upgrading)
            timeToUpgrade -= Time.deltaTime * 5;
        else
            timeToUpgrade = _timeToUpgrade;

        if(timeToUpgrade <= 0)
        {
            timeToUpgrade = _timeToUpgrade;

            cc.ApplyUpgrade(0);
        }

        if(healthUpgrades >= 5)
        {
            upgradeButtons[0].GetComponent<Button>().interactable = false;
        }
        if (maxAmmoUpgrades >= 7)
        {
            upgradeButtons[1].GetComponent<Button>().interactable = false;
        }
    }
}
