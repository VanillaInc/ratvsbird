﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyParam : MonoBehaviour {

    public diffParam dp;

    public void ApplyDifficulty()
    {
        FindObjectOfType<DifficultyManager>().Apply(dp);
    }

}
