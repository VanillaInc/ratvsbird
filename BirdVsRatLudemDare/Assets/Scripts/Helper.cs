﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Ran
{
    public static float ge(Range range)
    {
        return Random.Range(range.min, range.max);
    }

    public static float dom(float max)
    {
        return Random.Range(0, max);
    }
}

public static class Tim
{
    
}

[System.Serializable]
public class Range
{
    public float min;
    public float max;

    public Range(float mn, float mx)
    {
        min = mn;
        max = mx;
    }
}

[System.Serializable]
public class Timer
{
    public float length;
    public float RateOfDec;

    public Timer(float lengt, float rate)
    {
        length = lengt;
        RateOfDec = rate;
    }
}