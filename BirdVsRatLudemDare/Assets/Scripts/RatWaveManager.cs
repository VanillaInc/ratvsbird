﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatWaveManager : MonoBehaviour {

    public Animator plane;

    public Transform planeSpawn;

    public void PlaneDive()
    {
        plane.SetTrigger("Dive");
    }
}