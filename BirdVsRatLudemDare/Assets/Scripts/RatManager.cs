﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatManager : MonoBehaviour {

    public static bool firstRoundDone;

    public GameObject diagTrigger;
    public GameObject healButton;

    public float spawnCD = 0.2f;
    public float spawnCDremaining = 20;
    
    public bool waveFinished;
    bool canDie;
    public static bool firstWave;

    bool didSpawn;
    bool noEnemies;

    int totalNum;

    [System.Serializable]
    public class WaveComp {
        public GameObject enemyPrefab;
        public int num;
        public Transform spawnPoint;

        [System.NonSerialized]
        public int spawned = 0;
    }

    public WaveComp[] waves;

    private UIController ui;
    private ScoreManager sm;
    private HealthManager hm;
    private RatWaveManager rwm;

    GameObject[] go1;
    GameObject[] go2;
    GameObject[] go3;

    // Use this for initialization
    void Start () {

        rwm = FindObjectOfType<RatWaveManager>();
        hm = FindObjectOfType<HealthManager>();
        ui = FindObjectOfType<UIController>();
        sm = FindObjectOfType<ScoreManager>();
        spawnCDremaining = 10;

        foreach(WaveComp wc in waves)
        {
            totalNum += wc.num;
        }

        if(hm.value == hm.startValue && firstRoundDone)
        {
            ui.perfect.gameObject.SetActive(true);
            ui.perfect.GetComponent<Animator>().Play("perfectround");
            ui.TTNR.gameObject.SetActive(false);
            spawnCDremaining = 4f;
        }
    }

    // Update is called once per frame
    void Update() {

        go1 = GameObject.FindGameObjectsWithTag("Rat");
        go2 = GameObject.FindGameObjectsWithTag("DiveRat");
        go3 = GameObject.FindGameObjectsWithTag("RatBlimp");

        noEnemies = go1.Length == 0 && go2.Length == 0 && go3.Length == 0;

        waveFinished = sm.amountKilled >= totalNum && noEnemies;

        if(canDie && waveFinished)
        {

            if (transform.parent.childCount > 1)
            {
                GameObject nextW = transform.parent.GetChild(1).gameObject;
                nextW.SetActive(true);
                ui.TTNR.gameObject.SetActive(true);
                waveFinished = false;
            }
            else
            {
                // Last Wave!!

                Done();
            }

            if(!firstRoundDone)
            {
                firstRoundDone = true;
            }

            Destroy(gameObject);
        }

        ui.TTNR.text = spawnCDremaining.ToString("F3");

        if(!firstWave)
        {
            firstWave = true;
            spawnCDremaining = 1;
        }

        didSpawn = false;

        spawnCDremaining -= Time.deltaTime;

        if(spawnCDremaining <= 0)
        {
            spawnCDremaining = spawnCD;

            foreach(WaveComp wc in waves)
            {
                if(wc.spawned < wc.num)
                {
                    if(wc.spawnPoint.position == rwm.planeSpawn.position)
                    {
                        rwm.PlaneDive();
                    }

                    wc.spawned++;
                    Instantiate(wc.enemyPrefab, wc.spawnPoint.position, transform.rotation);
                    ui.TTNR.gameObject.SetActive(false);
                    ui.perfect.gameObject.SetActive(false);

                    didSpawn = true;

                    break;
                }
            }

            if(didSpawn == false)
            {
                canDie = true;
            }
        }
	}

    void Done()
    {
        healButton.SetActive(false);
        diagTrigger.GetComponent<NPC>().TriggerDialogue();
    }
}