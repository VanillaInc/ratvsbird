﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonShooter : MonoBehaviour {

    private CannonController cc;

    public ParticleSystem explosion;
    public ParticleSystem init;
    public ParticleSystem smoke;
    public AudioSource cannonSound;
    public AudioSource shootSound;
    public AudioSource explode;
    public AudioSource ratDie;

    public int maxAmmo;
    public int currentAmmo;
    public float reloadTime;
    public bool canFire;
    public bool isFiring;
    public Transform firePoint;

    public BirdBullet bullet;
    public float bulletSpeed;

    public float timeBetweenShots;
    private float shotCounter;

    public float timeBetweenReloads;
    public bool timeSet;

    bool reloading;
    float reloadSpeed;

	// Use this for initialization
	void Start () {
        cc = transform.parent.GetComponent<CannonController>();
        currentAmmo = maxAmmo;
        reloadSpeed = reloadTime;
	}
	
	// Update is called once per frame
	void Update () {

        // Stop reloading and firing at the same time
        if(timeBetweenReloads > -1)
        {
            timeBetweenReloads -= Time.deltaTime;
        }

        if(Time.timeScale < 1)
        {
            timeBetweenReloads = 0;
        }

        // Super fast reloads in slow motion
        if (Time.timeScale < 1)
        {
            reloadTime = 0;
        }
        else
        {
            reloadTime = reloadSpeed;
        }

        if (currentAmmo <= 0 || reloading)
            canFire = false;

        // Auto fire
        if (isFiring && canFire)
        {
            Counter();
        }
        else
        {
            shotCounter = 0;
        }

        if(Input.GetMouseButtonDown(1) && timeBetweenReloads <= 0 && !isFiring)
        {
            StartCoroutine(StartReloading());
        }

	}

    void Counter()
    {
        shotCounter -= Time.deltaTime;

        if(shotCounter <= 0)
        {
            shotCounter = timeBetweenShots;

            ShootBullet();
        }
    }

    void ShootBullet()
    {
        if(!timeSet)
        {
            timeBetweenReloads = cc.timeBetweenReloads;
            timeSet = true;
        }

        currentAmmo--;
        cannonSound.Play();
        shootSound.Play();
        BirdBullet newBullet = Instantiate(bullet, firePoint.position, firePoint.rotation) as BirdBullet;
        newBullet.speed = bulletSpeed;
        newBullet.explode = explode;
        newBullet.explosion = explosion;
    }

    IEnumerator StartReloading()
    {
        while (currentAmmo < maxAmmo)
        {
            reloading = true;

            Reload();

            yield return new WaitForSeconds(reloadTime);
        }

        reloading = false;
        timeSet = false;
    }

    void Reload()
    {
        currentAmmo++;
    }
}
