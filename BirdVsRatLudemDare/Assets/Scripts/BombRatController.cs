﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombRatController : MonoBehaviour {

    private Rigidbody2D rb;
    private Animator anim;

    public int scoreToGive;
    public float moveSpeed;

    private bool left;
    private bool hasBomb;

    public float bombTime;

    protected float parabAnimation;

    public GameObject bombObj;
    private GameObject bomb;
    private Transform bombPlace;

    public Range _walkTimer;
    private float walkTimer;
    public Range _walk2Timer;
    private float walk2Timer;

    bool parabola;

	// Use this for initialization
	void Start () {

        hasBomb = true;

        rb = GetComponent<Rigidbody2D>();

        GameObject leftSpawn = GameObject.Find("BombRatSpawnLeft");
        GameObject rightSpawn = GameObject.Find("BombRatSpawnRight");

        left = Vector3.Distance(transform.position, rightSpawn.transform.position) < Vector3.Distance(transform.position, leftSpawn.transform.position);

        bombPlace = transform.GetChild(0).transform;

        walkTimer = Ran.ge(_walkTimer);
        walk2Timer = Ran.ge(_walk2Timer);
	}
	
	// Update is called once per frame
	void Update () {

        parabAnimation += Time.deltaTime;
        parabAnimation = parabAnimation % 1;

        if(parabola)
        {
            bomb.transform.position = MathParabola.Parabola(bomb.transform.position, transform.position, 1, parabAnimation / 1);
        }

        if (walkTimer > 0)
        {
            walkTimer -= Time.deltaTime;

            // Walking animation

            rb.velocity = new Vector2(moveSpeed * Direction(), rb.velocity.y);
        }
        else if(hasBomb)
        {
            PlaceBomb();
        }

	}

    void PlaceBomb()
    {
        hasBomb = false;
        GameObject newBomb = Instantiate(bombObj, bombPlace.position, Quaternion.identity);
        newBomb.GetComponent<BombController>().rat = gameObject;
        bomb = newBomb;

        StartCoroutine(TimeBomb(bomb));

        StartCoroutine(RunAway());
    }

    IEnumerator RunAway()
    {
        left = !left;

        while (walk2Timer > 0)
        {
            walk2Timer -= Time.deltaTime;

            rb.velocity = new Vector2(moveSpeed * Direction(), rb.velocity.y);

            yield return null;
        }

        // Play hide animation
    }

    IEnumerator TimeBomb(GameObject bomb)
    {
        while(bombTime > 0)
        {
            bombTime -= Time.deltaTime;

            yield return null;
        }

        ExplodeBomb(bomb);
    }

    public void ShotBomb()
    {
        parabola = true;
    }

    void ExplodeBomb(GameObject bomb)
    {
        Animator bombAnim = bomb.GetComponent<Animator>();

        // Play Explosion
            // Sound Effect
            // Effect

        // Hurt player

        Destroy(bomb);
    }

    public void Die()
    {
        Destroy(gameObject);
    }

    void Leave()
    {

    }

    int Direction()
    {
        if (left)
            return -1;
        else
            return 1;
    }
}