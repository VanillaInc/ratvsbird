﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatController : MonoBehaviour {

    private ParticleSystem explosion;
    private ParticleSystem initExplosion;
    private ParticleSystem smoke;
    private AudioSource die;
    bool hasHit;

    public int score;
    public int damage;
    public float moveSpeed;
    public float fallSpeed;

    public float switchDirPerc;
    public float switchDirFreq;
    private float freqCounter;

    private Vector2 moveDirection;

    private Rigidbody2D rb;
    private HealthManager hm;
    private ScoreManager sm;
    private DifficultyParam dp;

	// Use this for initialization
	void Start () {
        dp = FindObjectOfType<DifficultyParam>();
        die = FindObjectOfType<CannonShooter>().ratDie;
        initExplosion = FindObjectOfType<CannonShooter>().init;
        smoke = FindObjectOfType<CannonShooter>().smoke;

        rb = GetComponent<Rigidbody2D>();
        hm = FindObjectOfType<HealthManager>();
        sm = FindObjectOfType<ScoreManager>();

        moveSpeed = DifficultyManager.dp.ratMoveSpeed;
        fallSpeed = DifficultyManager.dp.ratFallSpeed;

        moveDirection = new Vector2(moveSpeed, -fallSpeed);
	}
	
	// Update is called once per frame
	void Update () {
        rb.velocity = moveDirection;

        SwitchDirChance();
	}

    void SwitchDirChance()
    {
        freqCounter -= Time.deltaTime;

        if(freqCounter <= 0)
        {
            freqCounter = switchDirFreq;

            float rand = Random.Range(0, 100);
            rand *= 0.01f;

            if(rand <= switchDirPerc)
            {
                SwitchDir();
            }
        }
    }

    void SwitchDir()
    {
        transform.localScale = new Vector3(
            transform.localScale.x * -1,
            1,
            1
        );

        moveDirection = new Vector2(
            moveDirection.x * -1, 
            -fallSpeed
        );
    }

    public void PlayExplosion(ParticleSystem particleSystem)
    {
        Vector3 belowMe = new Vector3(transform.position.x, transform.position.y - 0.2f, transform.position.z);
        Instantiate(particleSystem, belowMe, Quaternion.identity);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "RatEnd")
        {
            if(!hasHit)
            {
                hm.RemoveHealth(damage);
                sm.amountKilled++;
                PlayExplosion(initExplosion);
                PlayExplosion(smoke);
                die.Play();
                Destroy(gameObject);
                hasHit = true;
            }
        }
    }
}
