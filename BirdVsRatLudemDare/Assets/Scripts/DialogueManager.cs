﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour {

    private Queue<string> sentances;
    private Queue<string> names;
    private Queue<Sprite> pfps;
    private Queue<bool> left;

    public GameObject healButton;

    private Dialogue[] dialogue;

    public GameObject dialogueBG;
    public TMP_Text nameText;
    public TMP_Text dialogueText;
    public Image pfp;

    public Transform pfpRight;
    public Transform pfpLeft;

    public Animator anim;

    public GameObject nextButton;

    bool transition;
    GameObject talker;

    //-----------------------------------------

    // Use this for initialization
    void Start () {
        dialogueBG.SetActive(false);
        pfp.gameObject.SetActive(false);
        sentances = new Queue<string>();
        names = new Queue<string>();
        pfps = new Queue<Sprite>();
        left = new Queue<bool>();
	}
	
    public void StartNewDialogueBranch(Dialogue[] dialoguevar, bool trans, GameObject recieve)
    {
        talker = recieve;
        healButton.SetActive(false);
        dialogue = dialoguevar;
        dialogueBG.SetActive(true);
        nextButton.SetActive(true);
        pfp.gameObject.SetActive(true);
        anim.SetBool("IsOpen", true);
        //pfp = profile;

        transition = trans;

        sentances.Clear();
        names.Clear();
        pfps.Clear();
        left.Clear();

        foreach(Dialogue d in dialogue)
        {
            left.Enqueue(d.left);
            sentances.Enqueue(d.sentance);
            names.Enqueue(d.name);
            pfps.Enqueue(d.pfp);
        }

        DisplayNextSentance();
    }

    public void DisplayNextSentance()
    {
        if(sentances.Count == 0)
        {
            EndConversation();
            return;
        }

        bool lefts = left.Dequeue();
        if (lefts)
            pfp.gameObject.transform.position = pfpLeft.position;
        else
            pfp.gameObject.transform.position = pfpRight.position;

        Sprite sprite = pfps.Dequeue();
        if(sprite != null)
            pfp.sprite = sprite;

        string nameString = names.Dequeue();
        nameText.text = nameString;
        string sentance = sentances.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentace(sentance));
    }

    IEnumerator TypeSentace (string sentance)
    {
        dialogueText.text = "";
        foreach(char letter in sentance.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    public void EndConversation()
    {
        pfp.gameObject.SetActive(false);
        anim.SetBool("IsOpen", false);
        healButton.SetActive(true);

        if(transition)
        {
            talker.GetComponent<DialogueToWave>().StartWave();
        }
    }
}
