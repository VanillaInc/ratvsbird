﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : MonoBehaviour {

    public float duration;

    private CannonController cc;

    private void Start()
    {
        cc = FindObjectOfType<CannonController>();
    }

    private void OnMouseDown()
    {
        cc.ApplyPowerUp("Rapid Fire", duration);
    }
}
