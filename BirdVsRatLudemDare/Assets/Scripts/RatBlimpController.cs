﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatBlimpController : MonoBehaviour {

    public AudioSource die;

    public int score;

    public float moveSpeed;

    public float _health;
    private float health;

    public Sprite normal;
    public Sprite hurt;

    public ParticleSystem smokeLittle;
    public ParticleSystem smokeALot;

    bool hurtBool;
    float tenth = 0.1f;

    private Animator anim;
    private ScoreManager sm;

    private void Start()
    {
        die = GameObject.Find("BlimpDie").GetComponent<AudioSource>();

        health = _health;

        sm = FindObjectOfType<ScoreManager>();
        anim = GetComponent<Animator>();

        smokeLittle = transform.GetChild(0).gameObject.GetComponent<ParticleSystem>();
        smokeALot = transform.GetChild(0).gameObject.GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        if(hurtBool)
        {
            tenth -= Time.deltaTime;

            if (tenth <= 0)
            {
                GetComponent<SpriteRenderer>().sprite = normal;
                tenth = 0.1f;
                hurtBool = false;
            }

        }

        transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);

        if (health <= 0)
        {
            Die();
        }

        if(health <= _health / 2)
        {
            smokeLittle.Play();
        }
    }

    public void Hurt()
    {

        GetComponent<SpriteRenderer>().sprite = hurt;
        health--;
        hurtBool = true;

    }

    void Die()
    {
        die.Play();
        FindObjectOfType<ScoreManager>().amountKilled++;
        anim.SetTrigger("Die");
        smokeALot.Play();
    }

    public void Destroy()
    {
        sm.AddScore(score);
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "RatEnd")
        {
            Destroy(gameObject);
        }
    }
}
