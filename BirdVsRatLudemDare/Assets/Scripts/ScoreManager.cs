﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    public AudioSource spawnSFX;

    public GameObject upgrader;
    public Transform upgradeSpot;
    public ParticleSystem spawn;

    public int score;
    public int amountKilled;

    private int subScore;

    private UpgradeManager um;

    private void Start()
    {
        um = FindObjectOfType<UpgradeManager>();
    }

    private void Update()
    {
        if(um != null)
        {
            if (subScore >= um.scoreToUpgrades[um.STUindex])
            {
                GameObject newUpper = Instantiate(upgrader, upgradeSpot.position, Quaternion.identity);
                Instantiate(spawn, newUpper.transform.position, Quaternion.identity);
                spawnSFX.Play();

                if (um.scoreToUpgrades.Length != um.STUindex - 1)
                    um.STUindex++;

                subScore = 0;
            }
        }
        else
        {
            um = FindObjectOfType<UpgradeManager>();
        }
    }

    public void AddScore(int scoreToAdd)
    {
        score += scoreToAdd;
        subScore += scoreToAdd;
    }
}
