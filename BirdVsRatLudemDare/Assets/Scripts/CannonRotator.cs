﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonRotator : MonoBehaviour {

    // Update is called once per frame
    void Update () {
        FaceMouse();
    }

    void FaceMouse()
    {
        // Get mouse pos
        Vector3 mousePos = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);

        // Get direction
        Vector2 direction = new Vector2(
                mousePos.x - transform.position.x,
                mousePos.y - transform.position.y
            );

        // Point in direction
        if (mousePos.y > transform.position.y)
        {
            transform.up = direction;
            FindObjectOfType<CannonShooter>().canFire = true;
        }
        else
        {
            FindObjectOfType<CannonShooter>().canFire = false;
        }
    }
}
