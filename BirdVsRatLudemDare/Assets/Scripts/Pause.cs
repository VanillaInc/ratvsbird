﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour {

    public static bool paused;

    //public GameObject pauseMenu;

    private CannonController cc;

    private void Start()
    {
        cc = FindObjectOfType<CannonController>();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)) //&& !paused)
        {
            //PauseMenu();
            SceneManager.LoadScene("MainMenu");
        }
    }

    /*
    public void PauseMenu()
    {
        //pauseMenu.SetActive(true);
        Time.timeScale = 0;
        paused = true;
        cc.cannon.canFire = false;
    }

    public void UnPause()
    {
        //pauseMenu.SetActive(false);
        Time.timeScale = 1;
        paused = false;
        cc.cannon.canFire = true;
    }
    */
}
