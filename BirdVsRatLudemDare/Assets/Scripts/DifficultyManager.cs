﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyManager : MonoBehaviour {

    public static diffParam dp;

    public static DifficultyManager dm;

    public void Apply(diffParam dpm)
    {
        dp = dpm;

        FindObjectOfType<HealthManager>().startValue = dp.startingHealth;
        FindObjectOfType<HealthManager>().value = FindObjectOfType<HealthManager>().startValue;
        FindObjectOfType<CannonController>().cannon.maxAmmo = dp.startingAmmo;
        FindObjectOfType<UpgradeManager>()._timeToUpgrade = dp.upgradeTime;
    }
}

[System.Serializable]
public class diffParam
{
    public float ratFallSpeed;
    public float ratMoveSpeed;
    public float diveRatFallSpeed;
    public float diveRatParachute;
    public int startingAmmo;
    public int startingHealth;
    public int upgradeTime;
}