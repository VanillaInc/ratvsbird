﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOptions : MonoBehaviour {

    public bool autoFire;

    public static bool gameOptionsExists;

	// Use this for initialization
	void Start () {
        if(gameOptionsExists)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            gameOptionsExists = true;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
