﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueToWave : MonoBehaviour {

    public GameObject wave;

    public void StartWave()
    {
        wave.SetActive(true);
    }
}
