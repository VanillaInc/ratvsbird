﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour {

    public GameObject healthButton;

    public int startValue;
    [System.NonSerialized]
    public int value;

    private UIController ui;

    public float _cooldown;
    private float cooldown;

	void Start () {
        value = startValue;
        ui = FindObjectOfType<UIController>();
	}

    private void Update()
    {
        if(cooldown > -1)
        {
            cooldown -= Time.deltaTime;
        }

        healthButton.GetComponent<Button>().interactable = cooldown <= 0;

        if(value > startValue)
        {
            value = startValue;
        }

        if(value <= 0)
        {
            ui.GameOver();
            value = 0;
        }
    }

    public void RemoveHealth(int healthToRemove)
    {
        if(value > 0)
            value -= healthToRemove;

        cooldown = _cooldown;
    }

    public void AddHealth(int healthToAdd)
    {
        if(value < startValue)
            value += healthToAdd;
    }
}
