﻿using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneChanger : MonoBehaviour {

    public Animator fadeBox;
    private string sceneToLoad;

    private void Start()
    {
        fadeBox.Play("FadeIn");
    }

    public void LoadScene(string scene)
    {
        fadeBox.SetTrigger("fadeOut");

        sceneToLoad = scene;
    }

    public void noTrans(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    void LoadTheSceneShithead()
    {
        SceneManager.LoadScene(sceneToLoad);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
