﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonController : MonoBehaviour {

    public CannonShooter cannon;

    public AudioSource collect;

    private UIController ui;
    private HealthManager hm;
    private UpgradeManager um;

    public float upgradeTimeSlow;
    public float bulletSpeedUpgrade;
    public int healthUpgrade;
    public bool canUpgrade;

    public float timeBetweenReloads;

    private bool poweringUp;
    private float powerUpTime;
    private float oldValue;

	// Use this for initialization
	void Start () {
        ui = FindObjectOfType<UIController>();
        hm = FindObjectOfType<HealthManager>();
        um = FindObjectOfType<UpgradeManager>();
    }
	
	// Update is called once per frame
	void Update () {

        if(poweringUp)
        {
            powerUpTime -= Time.deltaTime;
        }

		if(Input.GetMouseButton(0))
        {
            cannon.isFiring = true;
        }

        if(Input.GetMouseButtonUp(0))
        {
            cannon.isFiring = false;
        }

    }

    public void Upgrade()
    {
        // Prepare for upgrade

        cannon.currentAmmo = cannon.maxAmmo;
        collect.Play();
        ui.OpenUpgradeMenu();
        Time.timeScale = upgradeTimeSlow;
        canUpgrade = true;
    }

    public void ApplyUpgrade(int type)
    {
        // Change stats
        // 1: Health per click, 2: Max Health, 3: fire speed, 4: bullet speed

        UpgradeManager.upgrading = false;
        ui.upgradeMenu.SetActive(false);
        Time.timeScale = 1;

        if(type == 0)
        {
            // Uhhhhhhh
        }

        if(type == 1)
        {
            um.healthUpgrades++;
            ui.healthToAdd += 1;
            hm.startValue += healthUpgrade;
        }
        else if(type == 2)
        {
            um.maxAmmoUpgrades++;
            cannon.maxAmmo += 2;
            cannon.currentAmmo = cannon.maxAmmo;
            FindObjectOfType<VolunteerManager>().Refresh();

        }
    }

    public void ApplyPowerUp(string type, float duration)
    {
        if(poweringUp)
        {
            CancelPowerUp();
        }

        if(type == "Rapid Fire")
        {
            oldValue = cannon.timeBetweenShots;
            cannon.timeBetweenShots = 0.05f;
            poweringUp = true;
            powerUpTime = duration;

            StartCoroutine(PowerUp());
        }
    }

    IEnumerator PowerUp()
    {
        while (powerUpTime > 0)
        {
            yield return null;
        }

        cannon.timeBetweenShots = oldValue;
    }

    void CancelPowerUp()
    {
        poweringUp = false;
        powerUpTime = 0;
    }

    public void CanFire(bool b)
    {
        cannon.canFire = b;
    }
}
