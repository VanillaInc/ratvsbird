﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolunteerController : MonoBehaviour {

    private Animator anim;
    public ParticleSystem trans;
    public AudioSource regen;

    private VolunteerManager vm;

    public bool egg;
    public bool eggBK;

    private void Start()
    {
        Instantiate(regen, transform.position, transform.rotation);
        anim = GetComponent<Animator>();
        vm = FindObjectOfType<VolunteerManager>();
    }

    private void Update()
    {
        anim.SetBool("Egg", egg);
    }

    public void Egg()
    {
        Instantiate(trans, transform.position, Quaternion.identity);
        egg = true;
    }

    public void Idle()
    {
        regen.Play();
        Instantiate(trans, transform.position, Quaternion.identity);
        egg = false;
    }
}
