﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdBullet : MonoBehaviour {

    public ParticleSystem explosion;
    public AudioSource explode;
    public float speed;
    private float lifeTime = 5f;

    private ScoreManager sm;

	// Use this for initialization
	void Start () {
        sm = FindObjectOfType<ScoreManager>();
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.up * speed * Time.deltaTime);

        lifeTime -= Time.deltaTime;

        if(lifeTime <= 0)
        {
            Destroy(gameObject);
        }
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Rat")
        {
            sm.AddScore(other.GetComponent<RatController>().score);
            sm.amountKilled++;
            explode.Play();
            other.GetComponent<RatController>().PlayExplosion(explosion);
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
        if (other.tag == "DiveRat")
        {
            sm.AddScore(other.GetComponent<DiveRatController>().score);
            sm.amountKilled++;
            explode.Play();
            other.GetComponent<DiveRatController>().PlayExplosion(explosion);
            Destroy(other.gameObject);
            Destroy(gameObject);
        }

        if(other.tag == "Bomb")
        {
            BombController bc = other.GetComponent<BombController>();

            bc.StartCheck();
            bc.rat.GetComponent<BombRatController>().ShotBomb();
        }

        RatBlimpController rbc = other.GetComponent<RatBlimpController>();

        if(rbc != null)
        {
            rbc.Hurt();
            Destroy(gameObject);
        }
    }
}
