﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IntroScript : MonoBehaviour {

    public Text[] texts;
    public bool[] textFaded;
    private int textIndex = 0;

    public float timePerText;

    bool fading;

	// Use this for initialization
	void Start () {
        foreach(Text t in texts)
        {
            t.color = new Color(t.color.r, t.color.g, t.color.b, 0);
        }

        textFaded = new bool[texts.Length];
	}
	
	// Update is called once per frame
	void Update () {
        if(!fading && textIndex < texts.Length)
        {
            for(int i = 0; i < texts.Length; i++)
            {
                if (texts[textIndex].color.a == 0 && textFaded[textIndex] == false)
                {
                    StartCoroutine(FadeTextToFullAlpha(0.3f, texts[textIndex]));
                }
                fading = true;
            }
        }
        
        if(textIndex == texts.Length)
        {
            SceneManager.LoadScene("Level1");
        }
    }

    public IEnumerator FadeTextToFullAlpha(float t, Text i)
    {

        i.color = new Color(i.color.r, i.color.g, i.color.b, 0);
        while (i.color.a < 1.0f)
        {
            i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a + (Time.deltaTime / t));
            yield return null;
        }

        yield return new WaitForSeconds(timePerText);

        i.color = new Color(i.color.r, i.color.g, i.color.b, 1);
        while (i.color.a > 0.0f)
        {
            i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a - (Time.deltaTime / t));
            yield return null;
        }

        textFaded[textIndex] = true;
        fading = false;
        textIndex++;
    }
}
