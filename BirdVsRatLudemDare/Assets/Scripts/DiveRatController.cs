﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiveRatController : MonoBehaviour {

    private ParticleSystem explosion;
    private ParticleSystem initExplosion;
    private ParticleSystem smoke;
    private AudioSource die;
    bool hasHit;

    public int score;
    public int damage;
    public float fallSpeed;
    public float parachuteSpeed;

    bool parachute;

    private Rigidbody2D rb;
    private Animator anim;
    private HealthManager hm;
    private ScoreManager sm;

    // Use this for initialization
    void Start () {
        die = FindObjectOfType<CannonShooter>().ratDie;
        initExplosion = FindObjectOfType<CannonShooter>().init;
        smoke = FindObjectOfType<CannonShooter>().smoke;

        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        hm = FindObjectOfType<HealthManager>();
        sm = FindObjectOfType<ScoreManager>();

        parachuteSpeed = DifficultyManager.dp.diveRatParachute;
        fallSpeed = DifficultyManager.dp.diveRatFallSpeed;
    }

    // Update is called once per frame
    void Update () {

        if(parachute)
            transform.Translate(Vector3.down * fallSpeed * Time.deltaTime);
        else
            rb.gravityScale = fallSpeed;

	}

    public void OpenParachute()
    {
        rb.velocity = Vector3.zero;
        anim.SetTrigger("Open");
        fallSpeed = parachuteSpeed;
        rb.isKinematic = true;
        parachute = true;
    }

    public void PlayExplosion(ParticleSystem particleSystem)
    {
        Vector3 belowMe = new Vector3(transform.position.x, transform.position.y - 0.2f, transform.position.z);
        Instantiate(particleSystem, belowMe, Quaternion.identity);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "RatEnd")
        {
            if (!hasHit)
            {
                hm.RemoveHealth(damage);
                sm.amountKilled++;
                PlayExplosion(initExplosion);
                PlayExplosion(smoke);
                die.Play();
                Destroy(gameObject);
                hasHit = true;
            }
        }
    }
}
