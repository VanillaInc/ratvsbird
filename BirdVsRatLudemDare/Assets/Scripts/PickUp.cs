﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour {

    private GameObject upgrader;
    private bool pickedUp;

	// Use this for initialization
	void Start () {
        upgrader = GameObject.FindGameObjectWithTag("Upgrade");
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(0))
        { // if left button pressed...

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.tag == "Upgrade")
                {
                    pickedUp = true;
                }
            }
        }

        if(pickedUp)
        {
            Vector3 mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);
            upgrader.transform.position = new Vector3(mousePos.x, mousePos.y, 0);
        }
    }
}
