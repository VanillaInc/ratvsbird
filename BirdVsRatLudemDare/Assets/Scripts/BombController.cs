﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombController : MonoBehaviour {

    public GameObject rat;
    private BoxCollider2D box;

    private void Start()
    {
        box = GetComponent<BoxCollider2D>();
        box.enabled = false;
    }

    public void StartCheck()
    {
        box.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        BombRatController brc = other.GetComponent<BombRatController>();

        if(brc != null)
        {
            brc.Die();
        }
    }

}
